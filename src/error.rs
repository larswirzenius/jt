//! Errors returned from functions in this crate.

use std::path::PathBuf;

#[derive(Debug, thiserror::Error)]
pub enum JournalError {
    /// Configuration file does not exist.
    #[error("specified configuration file does not exist: {0}")]
    ConfigMissing(PathBuf),

    /// Failed to read the configuration file.
    ///
    /// This is for permission problems and such.
    #[error("failed to read configuration file {0}")]
    ReadConfig(PathBuf, #[source] std::io::Error),

    /// Configuration file has a syntax error.
    #[error("failed to understand configuration file syntax: {0}")]
    ConfigSyntax(PathBuf, #[source] serde_yaml::Error),

    /// The specified directory does not look like a journal.
    #[error("directory {0} is not a journal")]
    NotAJournal(String),

    /// The specified directory for the journal does not exist.
    #[error("journal directory does not exist: {0}")]
    NoJournalDirectory(PathBuf),

    /// Failed to create the directory for the journal.
    #[error("failed to create journal directory {0}")]
    CreateDirectory(PathBuf, #[source] std::io::Error),

    /// Failed to rename entry when finishing it.
    #[error("failed to rename journal entry {0} to {1}: {2}")]
    RenameEntry(PathBuf, PathBuf, #[source] std::io::Error),

    /// Failed to rename draft.
    #[error("failed to remove draft {0}: {1}")]
    RemoveDraft(PathBuf, #[source] std::io::Error),

    /// Failed to write entry.
    #[error("failed to create journal entry {0}: {1}")]
    WriteEntry(PathBuf, #[source] std::io::Error),

    /// Failed to write topic page.
    #[error("failed to create topic page {0}: {1}")]
    WriteTopic(PathBuf, #[source] std::io::Error),

    /// User chose a draft that doesn't exist.
    #[error("No draft {0} in {1}")]
    NoSuchDraft(String, PathBuf),

    /// Too many drafts.
    #[error("there are already {0} drafts in {1}, can't create more")]
    TooManyDrafts(usize, PathBuf),

    /// User chose a topic that doesn't exist.
    #[error("No topic page {0}")]
    NoSuchTopic(PathBuf),

    /// Failed to read draft.
    #[error("failed to drafts {0}: {1}")]
    ReadDraft(PathBuf, #[source] std::io::Error),

    /// Draft is not UTF8.
    #[error("draft {0} is not UTF8: {1}")]
    DraftNotUUtf8(PathBuf, #[source] std::string::FromUtf8Error),

    /// Failed to get metadata for specific file in drafts folder.
    #[error("failed to stat draft in {0}: {1}")]
    StatDraft(PathBuf, #[source] std::io::Error),

    /// Error spawning git.
    #[error("failed to start git in {0}: {1}")]
    SpawnGit(PathBuf, std::io::Error),

    /// Git init failed.
    #[error("git init failed in {0}: {1}")]
    GitInit(PathBuf, String),

    /// Git add failed.
    #[error("git add failed in {0}: {1}")]
    GitAdd(PathBuf, String),

    /// Git commit failed.
    #[error("git commit failed in {0}: {1}")]
    GitCommit(PathBuf, String),

    /// Error spawning editor.
    #[error("failed to start editor {0}: {1}")]
    SpawnEditor(PathBuf, #[source] std::io::Error),

    /// Editor failed.
    #[error("editor {0} failed: {1}")]
    EditorFailed(PathBuf, String),

    /// Template not found.
    #[error("template not found: {0}")]
    TemplateNotFound(String),

    /// Failed to render a Tera template.
    #[error("template {0} failed to render: {1}")]
    TemplateRender(String, #[source] tera::Error),

    /// Failed to make a path relative to a directory.
    #[error("failed to make {0} relative to {1}: {2}")]
    RelativePath(PathBuf, PathBuf, std::path::StripPrefixError),

    /// Problem with glob pattern.
    #[error("Error in glob pattern {0}: {1}")]
    PatternError(String, #[source] glob::PatternError),

    /// Problem when matching glob pattern on actual files.
    #[error("Failed to match glob pattern {0}: {1}")]
    GlobError(String, #[source] glob::GlobError),
}
