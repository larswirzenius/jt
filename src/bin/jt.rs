use jt::config::Configuration;
use jt::opt::{Opt, SubCommand};

use clap::Parser;

fn main() {
    if let Err(err) = do_work() {
        eprintln!("ERROR: {:?}", err);
        std::process::exit(1);
    }
}

fn do_work() -> anyhow::Result<()> {
    env_logger::init_from_env("JT_LOG");
    let opt = Opt::parse();
    let config = Configuration::read(&opt)?;
    match opt.cmd {
        SubCommand::Config(x) => x.run(&config)?,
        SubCommand::Init(x) => x.run(&config)?,
        SubCommand::IsJournal(x) => x.run(&config)?,
        SubCommand::New(x) => x.run(&config)?,
        SubCommand::NewTopic(x) => x.run(&config)?,
        SubCommand::List(x) => x.run(&config)?,
        SubCommand::Edit(x) => x.run(&config)?,
        SubCommand::Remove(x) => x.run(&config)?,
        SubCommand::Finish(x) => x.run(&config)?,
    }
    Ok(())
}
