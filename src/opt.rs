//! Command line options.

use crate::cmd;
use clap::Parser;
use std::path::PathBuf;

/// A parsed command line.
#[derive(Debug, Parser)]
#[clap(about = "maintain a journal")]
pub struct Opt {
    /// Global options, common for all subcommands.
    #[clap(flatten)]
    pub global: GlobalOptions,

    /// The subcommand.
    #[clap(subcommand)]
    pub cmd: SubCommand,
}

/// Global options.
///
/// These options are common to all subcommands.
#[derive(Debug, Parser)]
pub struct GlobalOptions {
    /// Which configuration file to read.
    #[structopt(short, long, help = "Configuration file")]
    pub config: Option<PathBuf>,

    /// Which directory to use for the journal.
    #[structopt(short, long, help = "Directory where journal should be stored")]
    pub dirname: Option<PathBuf>,

    /// Sub-directory in journal where new entries are put.
    #[structopt(long)]
    pub entries: Option<PathBuf>,

    /// Which editor to invoke for editing journal entry drafts.
    #[structopt(
        long,
        short,
        help = "Invoke EDITOR for user to edit draft journal entry"
    )]
    pub editor: Option<String>,
}

/// A subcommand.
#[derive(Debug, Parser)]
pub enum SubCommand {
    /// Show configuration.
    Config(cmd::Config),

    /// Create a new journal in the chosen directory.
    Init(cmd::Init),

    /// Check if a directory is a journal.
    IsJournal(cmd::IsJournal),

    /// Create draft for a new journal entry.
    New(cmd::New),

    /// List current drafts.
    List(cmd::List),

    /// Create topic page.
    NewTopic(cmd::NewTopic),

    /// Invoke editor on journal entry draft.
    Edit(cmd::Edit),

    /// Remove a journal entry draft.
    Remove(cmd::Remove),

    /// Finish a journal entry draft.
    Finish(cmd::Finish),
}
